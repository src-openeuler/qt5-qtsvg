%global qt_module qtsvg

Summary: 	Qt5 - Support for rendering and displaying SVG
Name:    	qt5-%{qt_module}
Version: 	5.15.10
Release: 	1

# See LGPL_EXCEPTIONS.txt, LICENSE.GPL3, respectively, for exception details
License: 	LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:     	http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: 	https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

Patch0:  	qtsvg-CVE-2023-32573.patch

BuildRequires:  make
BuildRequires:  qt5-qtbase-devel >= %{version}
BuildRequires:  pkgconfig(zlib)

BuildRequires:  qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}

%description
Scalable Vector Graphics (SVG) is an XML-based language for describing
two-dimensional vector graphics. Qt provides classes for rendering and
displaying SVG drawings in widgets and on other paint devices.

%package devel
Summary: 	Development files for %{name}
Requires: 	%{name}%{?_isa} = %{version}-%{release}
Requires: 	qt5-qtbase-devel%{?_isa}
%description devel
%{summary}.

%package examples
Summary: 	Programming examples for %{name}
Requires: 	%{name}%{?_isa} = %{version}-%{release}
%description examples
%{summary}.


%prep
%autosetup -n qtsvg-everywhere-src-%{version} -p1

%build
%{qmake_qt5}

%make_build

%install
make install INSTALL_ROOT=%{buildroot}

pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  if [ -f "$(basename ${prl_file} .prl).so" ]; then
    rm -fv "$(basename ${prl_file} .prl).la"
    sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
  fi
done
popd


%ldconfig_scriptlets

%files
%license LICENSE.*
%{_qt5_libdir}/libQt5Svg.so.5*
%{_qt5_plugindir}/iconengines/libqsvgicon.so
%{_qt5_plugindir}/imageformats/libqsvg.so
%{_qt5_libdir}/cmake/Qt5Gui/Qt5Gui_QSvg*Plugin.cmake

%files devel
%{_qt5_headerdir}/QtSvg/
%{_qt5_libdir}/libQt5Svg.so
%{_qt5_libdir}/libQt5Svg.prl
%dir %{_qt5_libdir}/cmake/Qt5Svg/
%{_qt5_libdir}/cmake/Qt5Svg/Qt5SvgConfig*.cmake
%{_qt5_libdir}/pkgconfig/Qt5Svg.pc
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_svg*.pri

%files examples
%{_qt5_examplesdir}/


%changelog
* Mon Aug 21 2023 huayadong <huayadong@kylinos.cn> - 5.15.10-1
- update to version 5.15.10-1

* Thu Jan 13 2022 wangkai <wangkai385@huawei.com> - 5.15.2-2
- Fix CVE-2021-45930

* Wed Dec 29 2021 peijiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Wed Dec 08 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 5.11.1-6
- Allow style without type attribute

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-5
- Fix Source0

* Fri Jan 10 2020 zhouyihang <zhouyihang1@huawei.com> - 5.11.1-4
- change the source to valid address

* Thu Nov 07 2019 yanzhihua <yanzhihua4@huawei.com> - 5.11.1-3
- Package init
